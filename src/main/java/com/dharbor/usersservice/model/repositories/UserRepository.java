package com.dharbor.usersservice.model.repositories;

import com.dharbor.usersservice.model.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Ma. Laura Chiri
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByFirstNameAndLastName(String firstName, String lastName);
}
