package com.dharbor.usersservice.model.builder;

import com.dharbor.usersservice.model.domain.User;
import com.dharbor.usersservice.model.response.UserResponse;

/**
 * @author Ma. Laura Chiri
 */
public final class UserResponseBuilder {

    private UserResponse instance;

    public static UserResponseBuilder getInstance(User user) {
        return(new UserResponseBuilder()).setUser(user);
    }

    private UserResponseBuilder() {
        this.instance = new UserResponse();
    }

    private UserResponseBuilder setUser(User user){
        instance.setId(user.getId());
        instance.setFirstName(user.getFirstName());
        instance.setLastName(user.getLastName());

        return this;
    }

    public UserResponse build() {
        return instance;
    }
}
