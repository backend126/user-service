package com.dharbor.usersservice.model.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author Ma. Laura Chiri
 */
@Setter
@Getter
public class UserResponse implements Serializable {

    private Long id;

    private String firstName;

    private String lastName;
}
