package com.dharbor.usersservice.model.input;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Ma. Laura Chiri
 */
@Setter
@Getter
public class UserCreateInput {

    private Long accountId;

    private String firstName;

    private String lastName;
}
