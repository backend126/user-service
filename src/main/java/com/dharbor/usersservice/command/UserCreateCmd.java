package com.dharbor.usersservice.command;

import com.dharbor.usersservice.exception.UserAlreadyExistsException;
import com.dharbor.usersservice.model.domain.User;
import com.dharbor.usersservice.model.input.UserCreateInput;
import com.dharbor.usersservice.model.repositories.UserRepository;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Ma. Laura Chiri
 */

@SynchronousExecution
public class UserCreateCmd implements BusinessLogicCommand {

    @Setter
    private UserCreateInput input;

    @Getter
    private User user;

    private UserRepository userRepository;

    public UserCreateCmd(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void execute() {

        validateAlreadyExistsUser(input.getFirstName(), input.getLastName());
        user = userRepository.save(composeUser(input));
    }

    private void validateAlreadyExistsUser(String firstName, String lastName) {
        User user = userRepository.findByFirstNameAndLastName(firstName, lastName);

        if(user != null){
            throw new UserAlreadyExistsException("User already exists!");
        }
    }

    private User composeUser(UserCreateInput input) {

        User instance = new User();
        instance.setAccountId(input.getAccountId());
        instance.setFirstName(input.getFirstName());
        instance.setLastName(input.getLastName());

        return instance;
    }
}
