package com.dharbor.usersservice.command;

import com.dharbor.usersservice.exception.UserNotFoundException;
import com.dharbor.usersservice.model.domain.User;
import com.dharbor.usersservice.model.repositories.UserRepository;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Ma. Laura Chiri
 */
@SynchronousExecution
public class UserReadCmd implements BusinessLogicCommand {

    @Setter
    private Long userId;

    @Getter
    private User user;

    private UserRepository userRepository;

    public UserReadCmd(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void execute() {
        user = userRepository.findById(userId).
            orElseThrow(()-> new UserNotFoundException("user not found by id:" + userId));
    }
}
