package com.dharbor.usersservice.controller;

import com.dharbor.usersservice.command.UserReadCmd;
import com.dharbor.usersservice.model.builder.UserResponseBuilder;
import com.dharbor.usersservice.model.response.UserResponse;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Ma. Laura Chiri
 */
@RequestMapping("/users")
@RequestScope
@RestController
public class UserReadController {

    private UserReadCmd userReadCmd;

    public UserReadController(UserReadCmd userReadCmd) {
        this.userReadCmd = userReadCmd;
    }

    @GetMapping(value="/{userId}")
    public UserResponse getById(@PathVariable("userId") Long userId){

        userReadCmd.setUserId(userId);
        userReadCmd.execute();

        return UserResponseBuilder.getInstance(userReadCmd.getUser()).build();
    }
}
