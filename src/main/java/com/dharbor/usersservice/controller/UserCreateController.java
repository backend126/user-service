package com.dharbor.usersservice.controller;

import com.dharbor.usersservice.command.UserCreateCmd;
import com.dharbor.usersservice.model.builder.UserResponseBuilder;
import com.dharbor.usersservice.model.response.UserResponse;
import com.dharbor.usersservice.model.input.UserCreateInput;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Ma. Laura Chiri
 */

@RestController
@RequestScope
@RequestMapping("/users")
public class UserCreateController {

    private UserCreateCmd userCreateCmd;

    public UserCreateController(UserCreateCmd userCreateCmd) {
        this.userCreateCmd = userCreateCmd;
    }

    @PostMapping
    public UserResponse createUser(@RequestBody UserCreateInput input) {
        userCreateCmd.setInput(input);
        userCreateCmd.execute();

        return UserResponseBuilder.getInstance(userCreateCmd.getUser()).build();
    }
}
