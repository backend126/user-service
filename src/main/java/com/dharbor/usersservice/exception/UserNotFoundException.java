package com.dharbor.usersservice.exception;

/**
 * @author Ma. Laura Chiri
 */
public class UserNotFoundException extends ApplicationException {
    public UserNotFoundException(String message){
        super(message);
    }
}
