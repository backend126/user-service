package com.dharbor.usersservice.exception;

/**
 * @author Ma. Laura Chiri
 */
public class UserAlreadyExistsException extends ApplicationException  {
    public UserAlreadyExistsException(String message){
        super(message);
    }
}
