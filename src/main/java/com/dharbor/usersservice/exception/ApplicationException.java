package com.dharbor.usersservice.exception;

/**
 * @author Ma. Laura Chiri
 */
public abstract class ApplicationException extends RuntimeException {

    public ApplicationException() {
    }

    public ApplicationException(String message) {
        super(message);
    }
}